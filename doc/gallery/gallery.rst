=======
Gallery
=======

Electron localisation function (ELF)
====================================

Water molecule (ELF=0.8):

.. image:: h2o-elf.png

:mod:`gpaw.elf`, :download:`h2o-elf.py`.


Wave-function isosurfaces for water
===================================

HOMO-3, HOMO-2, HOMO-1 and HOMO:

.. list-table::

   * - .. image:: homo-3.png
     - .. image:: homo-2.png
     - .. image:: homo-1.png
     - .. image:: homo-0.png

LUMO and LUMO+1:

===================== =====================
.. image:: lumo+0.png .. image:: lumo+1.png
===================== =====================

:download:`h2o-iso.py`.
